package exemple_encapsulacio;

/**
 * Aquesta versió de Circumferencia permet accedir a tots
 * els seus atributs directament.
 *
 */
public class CircumferenciaV1 {
	public final double area;
	public final double radi;
	public final double centreX;
	public final double centreY;

	public CircumferenciaV1(double radi) {
		this(0,0,radi);
	}

	public CircumferenciaV1(double centreX, double centreY) {
		this(centreX,centreY,1);
	}

	public CircumferenciaV1(double centreX, double centreY, double radi) {
		this.centreX = centreX;
		this.centreY = centreY;
		this.radi = radi;
		this.area = Math.PI * radi * radi;
	}
}
