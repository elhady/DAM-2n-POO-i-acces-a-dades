= Introducció al disseny orientat a objectes
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Exercicis

=== Diagrames de classes

1. Dibuixa un diagrama de classes que representi un llibre, definit per la
sentència següent: "Un llibre es compon d'un nombre de parts, que a la
vegada es componen d'un cert nombre de capítols. Els capítols estan
compostos de seccions."
+
Centra't només en les classes, relacions i multiplicitats.

2. Amplia el diagrama de classes de l'exercici anterior per incloure els
següents atributs.

** Un llibre inclou un editor, una data de publicació i un ISBN.
** Una part inclou un títol i un número.
** Un capítol inclou un títol, un número i un resum.
** Una secció inclou un títol i un número.

3. Considera el diagrama de classes de l'exercici anterior. Nota que les
classes *Part*, *Capítol* i *Secció* totes inclouen els atributs de *títol* i
*número*. Afegeix una classe abstracta i una relació de generalització per
traspassar aquests dos atributs a la classe abstracta.

4. Dibuixa un diagrama de classes que representi la relació entre pares i
fills. Tingues en compte que una persona pot tenir tant pares com fills.
Anota les associacions amb els noms dels rols i les multiplicitats.

5. Descriu la següent figura, identificant les classes i les seves
associacions:
+
image:imatges/exercici_treballador_habilitats.png[Diagrama de classes]
+
Actualitza el diagrama pas a pas per incloure els següents detalls:

** Quan un treballador té una habilitat, els anys d'experiència es
mantenen a la relació.
** Un treballador pot tenir un altre treballador com a encarregat, i un
treballador que és un encarregat s'ha de fer càrrec de cinc o més
treballadors. Donat un encarregat, es pot determinar qui està al seu
càrrec, però donat un treballador, no es pot saber qui és el seu encarregat.
** Una activitat no pot tenir més d'una activitat prèvia i qualsevol
nombres d'activitats posteriors. Utilitzant això, podem mostrar com
s'ordenen les activitats. Donada una activitat, es pot determinar quines
són les següents activitats (si n'hi ha) però no la seva activitat prèvia
(si en té una). Això és similar a com un equip pot estar fet de subequips
en el sentit en què cal tenir un equip abans per poder tenir subequips.
** Un treballador no està només associat a un conjunt d'habilitats,
sinó que un treballador té habilitats. Específicament, un treballador ha
de tenir tres o més habilitats, i qualsevol nombre de treballadors poden
tenir la mateixa habilitat.
** Un projecte no està només associat a un conjunt d'activitats, sinó
que un projecte conté activitats. Específicament, un projecte ha de tenir
una o més activitats, i una activitat ha de pertànyer a només un projecte.
** Els projectes i les activitats són tipus específics de feina.

6. Descriu la següent figura identificant les classes i les seves associacions:
+
image:imatges/exercici_treballador_habilitats2.png[Diagrama de classes]
+
Actualitza el diagrama pas a pas:

** Un pla pertany a un únic projecte i implica a un o més treballadors
i una o més activitats.
** Un treballador pot tenir zero o més plans, i un pla ha de pertànyer
a un únic treballador.
** Cada pla conté un únic calendari. Donat un pla, es pot determinar el
seu calendari, però donat un calendari, no es pot determina al pla
al qual pertany.
** Quan una activitat té un calendari, s'anomena una activitat programada,
i la data d'inici, la data de finalització, i la durada es mantenen per
a les activitats programades. Cada calendari pot tenir zero o més
activitats, però una activitats s'ha d'associar a un únic calendari.
** Una fita, un punt d'especial importància en el projecte, és un tipus
especial d'activitat programada en què hi ha zero o més productes del
treball que tenen un estatus específic. Una fita pot tenir qualsevol
nombre de productes associats, i un producte pot estar relacionat amb
qualsevol nombre de fites.

7. Volem informatitzar el funcionament d'una biblioteca. En concret, volem
un diagrama de classes per modelitzar els llibres de la biblioteca, els
seus socis i els préstecs que es produeixin.
+
Podem tenir un o més exemplars de cadascun dels llibres disponibles
a la biblioteca. Els exemplars constaran al catàleg de la biblioteca,
i es poden donar d'alta o de baixa.
+
Cada exemplar es pot trobar en un d'aquests estats: *disponible*, si es
pot prestar, *prestat*, si en aquests moments el té algun soci en
préstec, o *no disponible*, si per exemple s'ha perdut o s'està
reparant.
+
Els exemplars es poden prestar i es poden retornar després. Només es
poden prestar a socis i, evidentment, no es pot prestar el mateix
exemplar dues vegades al mateix temps.
+
Realitza el diagrama de classes d'aquest sistema, afegint els atributs
que consideris bàsics.

8. Llegeix l'link:zoo.md[enunciat del zoo] i fes un diagrama de
classes que mostri com resoldries aquest problema. Siguis tan detallat
com sigui possible.

=== Diagrames de seqüència

1. En una biblioteca es vol portar un control dels llibres existents,
dels socis de la biblioteca i dels préstecs que s’han dut a terme.
+
De cada llibre poden existir un o molts exemplars, així com cada
exemplar s’haurà de trobar en un catàleg. Per això, es podrà donar
entrada a un exemplar al catàleg o donar-li sortida.
+
A l’hora d’efectuar el préstec caldrà validar que existeix el soci
que el demana i la disponibilitat de l’exemplar. Les accions a fer
podran ser la de prestar un llibre i la de retornar-lo.
+
Es demana realitzar el diagrama de seqüència per a aquest enunciat.

2. Per a l'exercici del jardí, fes un diagrama de seqüència que mostri
els participants i els missatges que es realitzen en un dels *frames*.
Modelitza només l'accés a una de les posicions del jardí, i suposa
que en aquesta posició hi ha un *Altibus* d'alçada 8.

3. Per a l'exercici del zoo, fes un diagrama de seqüència que exemplifiqui
la resposta del sistema a una comanda “mira”. En aquest exemple concret,
suposarem que la comanda resulta en un animal realitzant una acció,
que l'animal resulta ser un cocodril, que l'acció que fa és alimenta,
i que l'altre animal que troba resulta ser una vaca.

=== Casos d'ús

1. Redacta el cas d'ús corresponent a la compra d'un bitllet de tren a un
terminal de rodalies. Fes el diagrama corresponent.

2. Redacta el cas d'ús corresponent al préstec d'un llibre en una biblioteca
pública. Fes el diagrama corresponent.
